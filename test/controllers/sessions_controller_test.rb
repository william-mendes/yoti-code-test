require 'test_helper'

class SecurityControllerTest < ActionDispatch::IntegrationTest
  test "should get login" do
    get login_url
    assert_response :found
  end

  test "should get logout" do
    get logout_url
    assert_response :found
  end
end
