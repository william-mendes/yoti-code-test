require 'test_helper'

class CommentsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @comment = comments(:one)
  end

  test "should create comment" do
    post comments_url, params: { comment: { 
      comment: @comment.comment,
      anonymous: @comment.anonymous,
      user_id: @comment.user_id } }
      
    assert_redirected_to root_url
  end

end
