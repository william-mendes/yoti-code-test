module SecurityHelper
    include UsersHelper

    def authenticate_yoti_user(yoti_token)

      yoti_activity_details = Yoti::Client.get_activity_details(yoti_token)
    
      if yoti_activity_details.outcome == 'SUCCESS'
        user_profile = yoti_activity_details.user_profile
        user = create_or_update_user(user_profile)
        session[:user_id] = user[:id]
        redirect_to root_url
      else
        render text: 'Error: Fetching the activity details failed.', status: :error
      end
    
    end
end
