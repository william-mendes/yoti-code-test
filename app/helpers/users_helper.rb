module UsersHelper
    def load_user
        @user = User.find_by_id(session[:user_id])
    end

    def create_or_update_user(user_profile)
        user = User.find_by_email(user_profile['email_address'])
    
        if user.nil?
          user = User.new(
            email: user_profile['email_address'],
            phone_number: user_profile['phone_number'])
          user.save
        else
          user[:phone_number] = user_profile['phone_number']
          user.save
        end
  
        save_selfie_file(user[:id], user_profile['selfie'])

        return user
    end

    def save_selfie_file(user_id, selfie)
        File.open(Rails.root.join('public/users', "#{user_id}.jpeg"), 'wb') { |file| file.write(selfie) }
    end
end