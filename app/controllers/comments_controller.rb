class CommentsController < ApplicationController
  def create
    @comment = Comment.new(comment_params)
    @comment[:user_id] = session[:user_id]

    if @comment.save
      redirect_to root_url, notice: 'Comment was successfully created.'
    else
      redirect_to root_url, errors: @comment.errors
    end
  end

  private
    def comment_params
      params.require(:comment).permit(:comment, :anonymous)
    end
end