class SecurityController < ApplicationController
  include SecurityHelper
    
  def new
    if not params[:token].nil?
      authenticate_yoti_user(params[:token])
    else
      redirect_to root_url
    end
  end
  
  def destroy
    reset_session
    redirect_to root_url
  end
end  