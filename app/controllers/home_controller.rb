class HomeController < ApplicationController
  include UsersHelper, CommentsHelper
  before_action :load_yoti, :load_user, :load_comments, only: [:index]

  def index
  end
end