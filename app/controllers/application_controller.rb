class ApplicationController < ActionController::Base
  include SecurityHelper, ApplicationHelper
  protect_from_forgery with: :exception
end  