# README

This repository is an example of how to integrate the Yoti SDK with an application.
The application is a single page, where users can post comments to a given subject.

**Application specifications**:

* Comments can be posted only by logged users

* Comments can be seen by anyone

* Posted comments can be anonymous, although it is persisted users information, which is related to the comments 1:MANY

* The user can specify if he/she wants to display personal information with the comment

* Login funcionality is only available by Yoti Login button

* Yoti login's call back uses https, in order to this application to work straight away with the configured call back, the pulma server which is runned by "rails server" needs an add-on to run with https self signed certificate to run in localhost


**Things to consider:**

* This example was created using the Ruby SDK

* For the sake of the example, it was used helpers to delegate resposabilites, for a different architecture a different layer could be used such as Application Services or Domain Services

* According to the specification, focus was given more to the funcionality instead of visual desing, the materialize css framework was used

* The application was created using sqllite for easily access by anyone

* Due to the time for the test, not all scenarios of test cases were created, few scenarios that would have to be created if more time were applied:
	- SecurityController - Login:
		- With params, authentication has been called
		- Without params, redirects to root
	- SecurityController - Logout:
		- When called: Session is cleaned root redirection is called
	- CommentsController
		- When create is called, checks the creation of the entity 
	- SecurityHelper
		- With valid params, client SDK is called and user_id is saved in session
		- With invalid params, renders error message
	- UsersHelper
		- Load_user returns a valid user
		- Create_or_update_user changes the count for creation as well as validade the entity created or updated
		- Save_selfie_file with valid binary checks if file was saved
		- Save_selfie_file with invalid warn user that was not possible to fetch the image from the api
		
		