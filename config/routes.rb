Rails.application.routes.draw do
  root "home#index"
  get "login" => "security#new"
  get "logout" => "security#destroy"

  post "comments" => "comments#create"
end